package com.bakaeva.tm.repository;

import com.bakaeva.tm.api.repository.ICommandRepository;
import com.bakaeva.tm.constant.ArgumentConst;
import com.bakaeva.tm.constant.TerminalConst;
import com.bakaeva.tm.model.Command;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {
    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, "Display terminal commands."
    );

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "Show developer info."
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, "Show version info."
    );

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO, "Display information about system."
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null, "Close application."
    );

    public static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Show program arguments."
    );

    public static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS, "Show program commands."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, "Create new task."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null, "Remove all tasks."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, "Show task list."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, "Create new project."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, "Remove all projects."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, "Show project list."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, ABOUT, VERSION, INFO, ARGUMENTS, COMMANDS, EXIT,
            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR
    };

    public final String[] commands = getCommands(TERMINAL_COMMANDS);

    public final String[] arguments = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (Command value : values) {
            final String name = value.getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (Command value : values) {
            final String argument = value.getArg();
            if (argument == null || argument.isEmpty()) continue;
            result[index] = argument;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    @Override
    public String[] getCommands() {
        return commands;
    }

    @Override
    public String[] getArgs() {
        return arguments;
    }
}

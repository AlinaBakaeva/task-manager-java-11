package com.bakaeva.tm.api.repository;

import com.bakaeva.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}

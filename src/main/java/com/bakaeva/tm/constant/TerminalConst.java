package com.bakaeva.tm.constant;

public interface TerminalConst {

    String HELP = "help";

    String VERSION = "version";

    String ABOUT = "about";

    String INFO = "info";

    String EXIT = "exit";

    String ARGUMENTS = "arguments";

    String COMMANDS = "commands";

    String TASK_LIST = "task-list";

    String TASK_CLEAR = "task-clear";

    String TASK_CREATE = "task-create";

    String PROJECT_LIST = "project-list";

    String PROJECT_CLEAR = "project-clear";

    String PROJECT_CREATE = "project-create";

}
